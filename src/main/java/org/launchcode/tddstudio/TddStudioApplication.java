package org.launchcode.tddstudio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TddStudioApplication {

	public static void main(String[] args) {
		SpringApplication.run(TddStudioApplication.class, args);
	}

}
